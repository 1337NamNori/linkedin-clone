import React, {useState} from 'react';

function BaseLink({text, color, fszClass, style, className}) {
    const baseClass = fszClass || 'fs-7';
    const [isHovered, setIsHovered] = useState(false);

    return (
        <div
            className={(isHovered ? baseClass + ' text-decoration-underline' : baseClass) + ' ' + (className ?? '')}
            style={{...style, color: color || 'var(--bs-dark)', cursor: 'pointer'}}
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
        >
            {text}
        </div>
    );
}

export default BaseLink;