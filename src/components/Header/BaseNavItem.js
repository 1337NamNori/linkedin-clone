import React, {useState} from 'react';
import {Image} from "react-bootstrap";

function BaseNavItem({icon, image, title, titleIcon, active}) {
    const baseClass = 'd-flex flex-column align-items-center fs-5 p-3 px-md-2 px-xl-3 pb-md-0 pt-md-2'
        + (active ? ' border-2 border-dark border-bottom' : '');

    const [isHovered, setIsHovered] = useState(false);

    return (
        <div
            className={(!isHovered && !active) ? baseClass + ' text-black-50' : baseClass}
            style={{cursor: 'pointer'}}
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
        >
            {image && <Image src={image} alt="" roundedCircle width={20} height={20}/>}
            {icon && <i className={icon}/>}
            {title && (
                <span className="fs-8 d-flex align-items-center">
                    {title}
                    {titleIcon && <i className={titleIcon + " ms-1 mb-1"}/>}
                </span>
            )}
        </div>
    );
}

export default BaseNavItem;