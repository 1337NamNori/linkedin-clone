import React from 'react';
import {Container, FormControl, Image, Nav, Navbar} from "react-bootstrap";
import {Link} from "react-router-dom";
import BaseNavItem from "./BaseNavItem";
import CustomNavDropdown from "./CustomNavDropdown";
import BaseLink from "../BaseLink";

function Header() {
    return (
        <Navbar bg="white" expand="lg" fixed="top" className="py-0 border border-bottom">
            <Container>
                <Link to="/feed">
                    <Navbar.Brand>
                        <Image src="/images/home-logo.svg" alt="" />
                    </Navbar.Brand>
                </Link>
                <Navbar.Toggle className="my-2"/>
                <Navbar.Collapse className="navbar-expand-sm">
                    <div className="d-flex align-items-center bg-blue-gray px-3 rounded text-black-50">
                        <i className="fa-solid fa-magnifying-glass me-1 fs-7" />
                        <FormControl placeholder="Search" className="bg-transparent shadow-none border-0 fs-7"/>
                    </div>
                </Navbar.Collapse>
                <Navbar.Collapse className="justify-content-end h-100">
                    <Nav className="ms-auto flex-row flex-wrap h-100">
                        <BaseNavItem
                            icon="fa-solid fa-house-chimney"
                            title="Home"
                            active
                        />
                        <BaseNavItem
                            icon="fa-solid fa-user-group"
                            title="My Network"
                        />
                        <BaseNavItem
                            icon="fa-solid fa-briefcase"
                            title="Jobs"
                        />
                        <BaseNavItem
                            icon="fa-solid fa-comment-dots"
                            title="Messaging"
                        />
                        <BaseNavItem
                            icon="fa-solid fa-bell"
                            title="Notification"
                        />
                        <CustomNavDropdown />
                        <div
                            className="d-none d-md-block border-end border-1 border-secondary opacity-25"
                            style={{height: "49px"}}
                        />
                        <BaseNavItem
                            icon="fa-solid fa-grip"
                            title="Work"
                            titleIcon="fa-solid fa-sort-down"
                        />
                        <BaseLink text="Try Premium for free" color="#915907" fszClass="fs-8"
                                  style={{maxWidth: '80px', textAlign: 'center'}}
                                  className="pt-md-2"
                        />
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default Header;