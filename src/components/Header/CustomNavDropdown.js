import React from 'react';
import {Button, Image, NavDropdown} from "react-bootstrap";
import BaseNavItem from "./BaseNavItem";
import BaseLink from "../BaseLink";
import {useSelector} from "react-redux";
import {selectUserAvatar, selectUserName} from "../../features/auth/authSlice";
import {useAuth} from "../../hooks/useAuth";

function CustomNavDropdown() {
    const {logoutHandler} = useAuth();
    const userName = useSelector(selectUserName);
    const userAvatar = useSelector(selectUserAvatar);

    const button = (
        <BaseNavItem title="Me" titleIcon="fa-solid fa-sort-down" image={userAvatar} />
    );

    return (
        <NavDropdown title={button} id="nav-dropdown" active align="end">
            <div className="px-3" style={{width: '250px'}}>
                <div className="d-flex align-items-center">
                    <Image
                        src={userAvatar}
                        roundedCircle
                        width={60} height={60}
                    />
                    <h6 className="fw-bold ms-1">{userName}</h6>
                </div>
                <Button variant="primary" className="btn-rounded btn-sm w-100 mt-2">
                    View Profile
                </Button>
            </div>
            <NavDropdown.Divider/>
            <div className="px-3">
                <h6 className="fw-bold">Account</h6>
                <BaseLink text="Settings & Privacy" color="rgba(0, 0, 0, 0.6)" />
                <BaseLink text="Help" color="rgba(0, 0, 0, 0.6)" />
                <BaseLink text="Language" color="rgba(0, 0, 0, 0.6)" />
            </div>
            <NavDropdown.Divider/>
            <div className="px-3">
                <h6 className="fw-bold">Manage</h6>
                <BaseLink text="Posts & Activity" color="rgba(0, 0, 0, 0.6)" />
                <BaseLink text="Job Posting Account" color="rgba(0, 0, 0, 0.6)" />
            </div>
            <NavDropdown.Divider/>
            <div className="px-3" onClick={logoutHandler}>
                <BaseLink text="Sign out" color="rgba(0, 0, 0, 0.6)"/>
            </div>
        </NavDropdown>
    );
}

export default CustomNavDropdown;