import React from 'react';
import {Button, Card} from "react-bootstrap";
import Account from "./Account";

function Aside() {
    return (
        <Card className="mt-5 mt-lg-0">
            <Card.Body className="d-flex justify-content-between align-items-center">
                <span className="fs-7 fw-bold">Add to your feed</span>
                <i className="fa-solid fa-circle-info"/>
            </Card.Body>
            <Card.Body className="py-0">
                <Account
                    avatar="/images/aside-react.jpeg"
                    name="React"
                    bio="React is a JavaScript library for building user interfaces created by Facebook."
                />
                <Account
                    avatar="/images/aside-firebase.jpeg"
                    name="Firebase"
                    bio="Information Technology & Services."
                />
                <Account
                    avatar="/images/aside-netlify.jpeg"
                    name="Netlify"
                    bio="Build, deploy and scale modern web applications"
                />
            </Card.Body>
            <Card.Body>
                <Button variant="secondary" className="btn-text btn-sm">
                    View all recommendations
                    <i className="fa-solid fa-arrow-right"/>
                </Button>
            </Card.Body>
        </Card>
    );
}

export default Aside;