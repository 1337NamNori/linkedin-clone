import React from 'react';
import {Button, Image} from "react-bootstrap";

function Account({avatar, name, bio}) {
    return (
        <div className="d-flex align-items-start py-2">
            <Image
                src={avatar}
                roundedCircle
                width={48}
                height={48}
                thumbnail
                style={{objectFit: 'cover'}}
            />
            <div className="ms-2">
                <h5 className="fs-7 fw-bold mb-0">{name}</h5>
                <p className="fs-9 text-muted lh-sm my-1">{bio}</p>
                <Button variant="secondary" className="btn-rounded">
                    <i className="fa-solid fa-plus me-1"/>
                    Follow
                </Button>
            </div>
        </div>
    );
}

export default Account;