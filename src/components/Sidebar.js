import React, {useState} from 'react';
import {Button, Card, Image, ListGroup} from "react-bootstrap";
import BaseLink from "./BaseLink";
import {useSelector} from "react-redux";
import {selectUserAvatar, selectUserName} from "../features/auth/authSlice";

function Sidebar() {
    const [expand, setExpand] = useState(false);

    const userName = useSelector(selectUserName);
    const userAvatar = useSelector(selectUserAvatar);

    const toggleExpand = () => {
        setExpand(prevState => !prevState);
    }

    return (
        <div className="fs-8 fw-bold">
            <Card>
                <Card.Img variant="top" src="/images/card-bg.svg" height={54} style={{objectFit: 'cover'}}/>
                <ListGroup className="list-group-flush">
                    <ListGroup.Item
                        className="position-relative d-flex flex-column align-items-center"
                        style={{height: '115px'}}
                    >
                        <div
                            className="border border-2 border-white rounded-circle position-absolute top-0
                                       translate-middle-y bg-white">
                            <Image
                                src={userAvatar}
                                width={70} height={70}
                                roundedCircle
                                style={{objectFit: 'cover'}}
                            />
                        </div>
                        <BaseLink text={userName} className="mt-5" fszClass="fs-6"/>
                    </ListGroup.Item>
                    <ListGroup.Item
                        className={(!expand ? "d-none" : "d-flex") +
                                    " hover-bg-gray d-md-flex justify-content-between align-items-center"}
                    >
                        <div className="d-flex flex-column">
                           <span className="text-secondary">Connections</span>
                           <span>Grow your network</span>
                        </div>
                        <i className="fa-solid fa-user-plus"/>
                    </ListGroup.Item>
                    <ListGroup.Item className={(!expand ? "d-none" : "d-block") + " d-md-block hover-bg-gray"}>
                        <small className="text-muted fw-normal">
                            Access exclusive tools & insights
                        </small>
                        <div className="d-flex align-items-center">
                            <Image src="/images/premium.svg" alt="" width={16} className="me-1"/>
                            <span className="hover-color-primary">Try Premium for free</span>
                        </div>
                    </ListGroup.Item>
                    <ListGroup.Item className={(!expand ? "d-none" : "d-block") + " d-md-block hover-bg-gray"}>
                        <i className="fa-solid fa-bookmark me-2 text-secondary" />
                        My items
                    </ListGroup.Item>
                </ListGroup>
            </Card>
            <Card className={(!expand ? "d-none" : "d-block") + " d-md-block mt-2"}>
                <ListGroup className="list-group-flush">
                    <ListGroup.Item className="d-flex align-items-center justify-content-between">
                        <div>
                            <BaseLink text="Groups" color="var(--primary-color)"
                                      fszClass="fs-8" className="fw-bold pb-1"/>
                            <BaseLink text="Events" color="var(--primary-color)"
                                      fszClass="fs-8" className="fw-bold py-1"/>
                            <BaseLink text="Followed Hashtags" color="var(--primary-color)"
                                      fszClass="fs-8" className="fw-bold pt-1"/>
                        </div>
                        <Button variant="secondary" className="btn-text rounded-circle p-2">
                            <i className="fa-solid fa-plus text-dark" />
                        </Button>
                    </ListGroup.Item>
                    <ListGroup.Item className="d-flex justify-content-center align-items-center
                                               fs-7 text-secondary fw-bold hover-bg-gray"
                    >
                        Discover more
                    </ListGroup.Item>
                </ListGroup>
            </Card>
            <Button variant="secondary" className="btn-text w-100 btn-sm my-2 d-md-none" onClick={toggleExpand}>
                {expand ? "Show less" : "Show more"}
                <i className={"fa-solid ms-2 " + (expand ? "fa-angle-up" : "fa-angle-down")} />
            </Button>
        </div>
    );
}

export default Sidebar;