import React from 'react';
import Item from "./Item";
import {Image} from "react-bootstrap";

function Footer(props) {
    return (
        <div className="d-flex flex-column align-items-center py-3">
            <div className="px-4 d-flex flex-wrap justify-content-center align-items-center">
                <Item text="About" />
                <Item text="Accessibility" />
                <Item text="Help Center" />
                <Item text="Privacy & Terms" dropdown/>
                <Item text="Ad Choices" />
                <Item text="Advertising" />
                <Item text="Business Services" dropdown/>
                <Item text="Get the LinkedIn app" />
                <Item text="More" />
            </div>
            <div className="px-2 pt-2 text-center">
                <Image src="/images/login-logo.svg" height={14}/>
                <span className="ms-1 fs-9">LinkedIn Corporation © 2021</span>
            </div>
        </div>
    );
}

export default Footer;