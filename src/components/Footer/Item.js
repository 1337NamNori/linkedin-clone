import React from 'react';
import BaseLink from "../BaseLink";

function Item({text, dropdown}) {
    return (
        <span className="px-2 py-1 d-flex align-items-center hover-color-primary">
            <BaseLink
                text={text}
                color="var(--secondary-color)"
                fszClass="fs-9"
                className="hover-color-primary"
            />
            {dropdown && <i className="fa-solid fa-chevron-down ms-1 fs-9 mb-1"/>}
        </span>
    );
}

export default Item;