import React from 'react';
import {Image, OverlayTrigger, Popover, Tooltip} from "react-bootstrap";

export default (
    <Popover className="rounded-pill shadow-lg" style={{maxWidth: '400px'}}>
        <Popover.Body className="d-flex">
            <OverlayTrigger trigger={["hover", "focus"]} placement="top" overlay={<Tooltip>Like</Tooltip>}>
                <Image
                    src="/images/react-like-lg.png"
                    width={32} height={32}
                    className="mx-2 cursor-pointer react-icon react-icon-1"
                    style={{objectFit: 'contain'}}
                />
            </OverlayTrigger>
            <OverlayTrigger trigger={["hover", "focus"]} placement="top" overlay={<Tooltip>Celebrate</Tooltip>}>
                <Image
                    src="/images/react-celebrate-lg.png"
                    width={32} height={32}
                    className="mx-2 cursor-pointer react-icon react-icon-2"
                    style={{objectFit: 'contain'}}
                />
            </OverlayTrigger>
                <OverlayTrigger trigger={["hover", "focus"]} placement="top" overlay={<Tooltip>Support</Tooltip>}>
                    <Image
                    src="/images/react-support-lg.png"
                    width={32} height={32}
                    className="mx-2 cursor-pointer react-icon react-icon-3"
                    style={{objectFit: 'contain'}}
                />
            </OverlayTrigger>
            <OverlayTrigger trigger={["hover", "focus"]} placement="top" overlay={<Tooltip>Love</Tooltip>}>
                <Image
                    src="/images/react-love-lg.png"
                    width={32} height={32}
                    className="mx-2 cursor-pointer react-icon react-icon-4"
                    style={{objectFit: 'contain'}}
                />
            </OverlayTrigger>
            <OverlayTrigger trigger={["hover", "focus"]} placement="top" overlay={<Tooltip>Insightful</Tooltip>}>
                <Image
                    src="/images/react-insightful-lg.png"
                    width={32} height={32}
                    className="mx-2 cursor-pointer react-icon react-icon-5"
                    style={{objectFit: 'contain'}}
                />
            </OverlayTrigger>
            <OverlayTrigger trigger={["hover", "focus"]} placement="top" overlay={<Tooltip>Curious</Tooltip>}>
                <Image
                    src="/images/react-curious-lg.png"
                    width={32} height={32}
                    className="mx-2 cursor-pointer react-icon react-icon-6"
                    style={{objectFit: 'contain'}}
                />
            </OverlayTrigger>
        </Popover.Body>
    </Popover>
);