import React from 'react';

function OptionItem({icon, iconType, title, description, onClick}) {
    return (
        <div className="d-flex align-items-center py-2 px-3 hover-bg-gray" style={{height: '50px'}} onClick={onClick}>
            <div className="text-end" style={{width: '20px'}}>
                <i className={`fa-${iconType ? iconType : 'solid'} fa-${icon} text-secondary fs-5`}/>
            </div>
            <div className="ms-3 d-flex flex-column">
                <span className="fw-bold">{title}</span>
                <small className="text-muted fs-8 lh-1">{description}</small>
            </div>
        </div>
    );
}

export default OptionItem;