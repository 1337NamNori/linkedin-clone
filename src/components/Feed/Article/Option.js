import React, {useEffect, useState} from 'react';
import {Button, Dropdown, NavItem} from "react-bootstrap";
import OptionItem from "./OptionItem";
import {useSelector} from "react-redux";
import {selectUserID} from "../../../features/auth/authSlice";
import DeleteModal from "./DeleteModal";

function Option({author, articleID}) {
    const userID = useSelector(selectUserID);
    const [showModal, setShowModal] = useState(false);

    useEffect(() => {
        return () => setShowModal(false);
    }, [])

    const handleShowModal = () => setShowModal(true);
    const handleCloseModal = () => setShowModal(false);

    return (
        <Dropdown>
            <Dropdown.Toggle as={NavItem}>
                <Button variant="secondary" className="btn-text rounded-circle p-2">
                    <i className="fa-solid fa-ellipsis fs-5"/>
                </Button>
            </Dropdown.Toggle>
            <Dropdown.Menu className="py-1" style={{width: '300px'}}>
                <OptionItem
                    icon="bookmark"
                    iconType="regular"
                    title="Save"
                    description="Save for later"
                />
                <OptionItem
                    icon="link"
                    title="Copy link to post"
                />
                {(author.uid === userID) && (
                    <>
                        <OptionItem
                            icon="trash"
                            title="Delete post"
                            onClick={handleShowModal}
                        />
                        <DeleteModal show={showModal} handleClose={handleCloseModal} articleID={articleID}/>
                    </>
                )}
            </Dropdown.Menu>
        </Dropdown>
    );
}

export default Option;