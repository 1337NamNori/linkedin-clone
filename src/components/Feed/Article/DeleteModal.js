import React, {useEffect, useState} from 'react';
import {Button, Modal, Spinner} from "react-bootstrap";
import db from "../../../config/firebase";

function DeleteModal({handleClose, show, articleID}) {
    const [isDeleting, setIsDeleting] = useState(false);

    useEffect(() => {
        return () => {
            setIsDeleting(false);
        };
    }, []);

    const handleDelete = async () => {
        setIsDeleting(true);
        await db.collection("articles").doc(articleID).delete();
        setIsDeleting(false);
    }

    return (
        <Modal show={show} onHide={handleClose} animation={false} className="text-center">
            <Modal.Header>
                <Modal.Title>Delete post?</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>Are you sure you want to permanently remove this post from LinkedIn?</p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>Cancel</Button>
                <Button variant="outline-danger" onClick={handleDelete} disabled={isDeleting}>
                    {isDeleting && <Spinner animation="border" />}
                    Delete
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default DeleteModal;