import React from 'react';
import {Button, Card, Image, OverlayTrigger, Ratio} from "react-bootstrap";
import BaseLink from "../../BaseLink";
import Option from "./Option";
import ReactionWrap from "./ReactionWrap";
import {getEmbedUrl} from "../../../helpers/getEmbedYoutube";
import Moment from "react-moment";

function Article(props) {
    const {author, description, sharedImg, comments, video, id, createdAt} = props.article;

    return (
        <Card className="my-2">
            <Card.Body className="d-flex justify-content-between cursor-pointer">
                <div className="d-flex">
                    <Image
                        roundedCircle width={48} height={48}
                        src={author.avatar}
                    />
                    <div className="d-flex flex-column ms-2">
                        <strong className="lh-sm">{author.name}</strong>
                        <small className="text-muted lh-sm">
                            <Moment fromNow>{createdAt.toDate()}</Moment>
                            <span className="mx-1">•</span>
                            <i className="fa-solid fa-earth-americas" /></small>
                    </div>
                </div>
                <Option author={author} articleID={id}/>
            </Card.Body>
            {(description || (video && sharedImg)) && (
                <Card.Body className="py-0 fs-7 ">
                    <p className="p-0 m-0">
                        {description}
                    </p>
                    {video && <a href={video} target="_blank" rel="noreferrer">{video}</a>}
                </Card.Body>
            )}
            {sharedImg && <Card.Img src={sharedImg} className="mt-2"/>}
            {(video && !sharedImg) && (
                <Card.Body className="px-0 pb-0">
                    <div className="w-100 h-auto">
                        <Ratio aspectRatio="16x9">
                            <iframe
                                src={getEmbedUrl(video)}
                                width="100%" height="100%"
                                title="YouTube video player" frameBorder="0"
                                allow="accelerometer; autoplay; clipboard-write;
                                               encrypted-media; gyroscope; picture-in-picture"
                                allowFullScreen
                            />
                        </Ratio>
                    </div>
                </Card.Body>
            )}
            <Card.Body className="d-flex justify-content-between align-items-center">
                <div className="hover-color-primary fs-9 d-flex align-items-center hover-underline">
                    <Image src="/images/react-like.svg" width={16} height={16} />
                    <Image src="/images/react-celebrate.svg" width={16} height={16} />
                    <Image src="/images/react-heart.svg" width={16} height={16} />
                    <span className="ms-1">19</span>
                </div>
                <BaseLink text={`${comments} comments`} className="hover-color-primary" fszClass="fs-9" />
            </Card.Body>
            <hr className="m-0 mx-3"/>
            <Card.Body className="d-flex justify-content-between align-items-center py-2">
                <OverlayTrigger trigger="click" placement="top" overlay={ReactionWrap}>
                    <Button variant="secondary" className="btn-text flex-grow-1 flex-shrink-1 py-3">
                        <i className="fs-5 fa-regular fa-thumbs-up"/>
                        <span className="d-none d-sm-block ms-2">Like</span>
                    </Button>
                </OverlayTrigger>
                <Button variant="secondary" className="btn-text flex-grow-1 flex-shrink-1 py-3">
                    <i className="fs-5 fa-regular fa-comment-dots"/>
                    <span className="d-none d-sm-block ms-2">Comment</span>
                </Button>
                <Button variant="secondary" className="btn-text flex-grow-1 flex-shrink-1 py-3">
                    <i className="fs-5 fa-solid fa-share"/>
                    <span className="d-none d-sm-block ms-2">Share</span>
                </Button>
                <Button variant="secondary" className="btn-text flex-grow-1 flex-shrink-1 py-3">
                    <i className="fs-5 fa-solid fa-paper-plane"/>
                    <span className="d-none d-sm-block ms-2">Send</span>
                </Button>
            </Card.Body>
        </Card>
    );
}

export default Article;