import React from 'react';
import {Image} from "react-bootstrap";

function Item({image, title}) {
    return (
        <div className="d-flex hover-bg-gray p-2 rounded-3">
            <Image src={image} width={24}/>
            <span className="ms-2">{title}</span>
        </div>
    );
}

export default Item;