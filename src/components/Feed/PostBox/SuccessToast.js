import React, {useEffect, useState} from 'react';
import {Button, Toast, ToastContainer} from "react-bootstrap";
import BaseLink from "../../BaseLink";

function SuccessToast({postingDone}) {
    const [showToast, setShowToast] = useState(false);

    useEffect(() => {
        if (postingDone) {
            setShowToast(true);
        }
    }, [postingDone]);

    return (
        <ToastContainer position="bottom-start" className="p-3 position-fixed">
            <Toast show={showToast} onClose={() => setShowToast(false)} className="rounded-3"
                   delay={3000} autohide
            >
                <Toast.Body className="d-flex align-items-center py-3 justify-content-between">
                    <div className="d-flex align-items-center">
                        <i className="fa-solid fa-circle-check text-success fs-6 me-2"/>
                        <span className="fw-normal text-dark">Post successful.</span>
                        <BaseLink text="View" color="var(--primary-color)" className="fw-bold ms-1"/>
                    </div>
                    <Button variant="secondary" className="btn-text rounded-circle"
                            style={{width: '32px', height: '32px'}}
                            onClick={() => setShowToast(false)}
                    >
                        <i className="fa-solid fa-xmark"/>
                    </Button>
                </Toast.Body>
            </Toast>
        </ToastContainer>
    );
}

export default SuccessToast;