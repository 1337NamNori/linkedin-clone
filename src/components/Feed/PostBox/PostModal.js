import React, {useEffect, useRef, useState} from 'react';
import {useSelector} from "react-redux";
import {Button, FormControl, Image, InputGroup, Modal, Ratio, Spinner} from "react-bootstrap";
import autosize from 'autosize';
import firebase from 'firebase';

import {getEmbedUrl} from "../../../helpers/getEmbedYoutube";
import {selectUserAvatar, selectUserID, selectUserName} from "../../../features/auth/authSlice";

function PostModal({show, handleClose, isPosting, postingDone, setPostingDone, dispatchPostArticle}) {

    const textareaRef = useRef();
    const imageInput = useRef();

    const userID = useSelector(selectUserID);
    const userName = useSelector(selectUserName);
    const userAvatar = useSelector(selectUserAvatar);

    const [postContent, setPostContent] = useState('');
    const [postImage, setPostImage] = useState('');
    const [postVideoUrl, setPostVideoUrl] = useState('');
    const [showVideoInput, setShowVideoInput] = useState(false);
    const [canPost, setCanPost] = useState(false);

    const addHashtag = () => {
        setPostContent(prevState => ((prevState === '') ? prevState + '#' : prevState + ' #'));
        textareaRef.current.focus();
    }

    const handleImageInput = (e) => {
        if (e.target.files.length <= 0) return;

        const file = e.target.files[0];
        file.preview = URL.createObjectURL(file);
        setPostImage(file);

        e.target.value = null;
    }

    const clear = () => {
        setPostContent('');
        setPostImage('');
        setPostVideoUrl('');
        setShowVideoInput(false);
    }

    const postArticle = (e) => {
        e.preventDefault();
        if (e.target !== e.currentTarget) {
            return;
        }

        const payload = {
            userID,
            userName,
            userAvatar,
            image: postImage,
            video: postVideoUrl,
            description: postContent,
            createdAt: firebase.firestore.Timestamp.now(),
        }

        dispatchPostArticle(payload);
    }

    useEffect(() => {
        autosize(textareaRef.current);
    }, []);

    useEffect(() => {
        setCanPost((postContent || postImage || postVideoUrl))
    },[postImage, postContent, postVideoUrl])

    useEffect(() =>{
        if (postingDone) {
            clear();
            handleClose();
            setPostingDone(false);
        }
    }, [postingDone, setPostingDone, handleClose]);

    return (
        <Modal show={show} onHide={handleClose} animation={false} fullscreen="sm-down">
            <Modal.Header closeButton className="py-2 py-sm-3">
                <Modal.Title className="fw-normal fs-5">Create a post</Modal.Title>
            </Modal.Header>
            <Modal.Body className="d-flex align-items-center flex-grow-0 overflow-hidden py-2">
                <Image
                    src={userAvatar}
                    roundedCircle
                    width={48} height={48}
                />
                <div className="ms-2">
                    <p className="fw-bold fs-7 m-0">{userName}</p>
                    <Button variant="secondary" className="btn-rounded fs-7 py-1 px-2">
                        <i className="fa-solid fa-earth-americas me-1 fs-8"/>
                        Anyone
                        <i className="fa-solid fa-sort-down ms-1 mb-1 fs-8"/>
                    </Button>
                </div>
            </Modal.Body>
            <Modal.Body className="flex-grow-0 overflow-hidden py-2">
                <FormControl
                    ref={textareaRef}
                    as="textarea" placeholder="What do you want to talk about?"
                    className="border-0 p-0"
                    autoFocus
                    style={{maxHeight: '400px', minHeight: '100px'}}
                    value={postContent}
                    onChange={(e) => setPostContent(e.target.value)}
                />
            </Modal.Body>
            <input
                type="file" hidden accept="image/png, image/jpeg, image/svg" ref={imageInput}
                onChange={handleImageInput}
            />
            <Modal.Body/>
            {postImage && (
                <Modal.Body className="rounded-3 bg-danger position-relative p-0 flex-grow-0 overflow-hidden"
                            style={{maxHeight: '400px'}}
                >
                    <Image
                        src={postImage.preview}
                        className="w-100 h-100"
                        style={{objectFit: 'cover'}}
                    />
                    <div
                        className="position-absolute p-2 d-flex justify-content-end align-items-center top-0 end-0"
                    >
                        <div className="image-btn me-2" onClick={() => imageInput.current.click()}>
                            <i className="fa-solid fa-pencil"/>
                        </div>
                        <div className="image-btn" onClick={() => setPostImage('')}>
                            <i className="fa-solid fa-xmark"/>
                        </div>
                    </div>
                </Modal.Body>
            )}
            {showVideoInput && (
                <Modal.Body className="flex-grow-0 overflow-hidden py-2">
                    <InputGroup className="mb-3">
                        <FormControl
                            placeholder="Insert video URL"
                            value={postVideoUrl}
                            onChange={(e) => setPostVideoUrl(e.target.value)}
                        />
                        <Button
                            variant="secondary"
                            onClick={() => {setShowVideoInput(false); setPostVideoUrl('')}}
                        >
                            Cancel
                        </Button>
                    </InputGroup>
                    {postVideoUrl && (
                        <div className="w-100 h-auto">
                            <Ratio aspectRatio="16x9">
                                <iframe
                                    src={getEmbedUrl(postVideoUrl)}
                                    width="100%" height="100%"
                                    title="YouTube video player" frameBorder="0"
                                    allow="accelerometer; autoplay; clipboard-write;
                                           encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen
                                />
                            </Ratio>
                        </div>
                    )}
                </Modal.Body>
            )}
            <Modal.Body className="flex-grow-0 overflow-hidden py-2">
                <Button variant="primary" className="btn-text" onClick={addHashtag}>Add hashtag</Button>
            </Modal.Body>
            <Modal.Body className="d-flex justify-content-between align-items-center
                                   flex-grow-0 overflow-hidden pt-2 pb-3">
                <div className="d-flex align-items-center">
                    <Button
                        variant="secondary" className="btn-text rounded-circle p-2"
                        onClick={() => imageInput.current.click()}
                    >
                        <i className="fa-regular fa-image fs-5 text-secondary"/>
                    </Button>
                    <Button
                        variant="secondary" className="btn-text rounded-circle p-2"
                        onClick={() => setShowVideoInput(true)}
                    >
                        <i className="fa-brands fa-youtube fs-5 text-secondary"/>
                    </Button>
                    <div className="bg-black bg-opacity-25 mx-1" style={{height: '25px', width: '1px'}}/>
                    <div
                        className="hover-bg-gray ms-1 py-1 px-2 d-flex justify-content-center align-items-center
                                  fs-7 fw-bold text-secondary rounded-pill"
                    >
                        <i className="fa-regular fa-comment-dots fs-7 mt-1 me-1"/>
                        Anyone
                    </div>
                </div>
                <Button
                    variant={(!canPost || isPosting) ? "secondary" : "primary"}
                    disabled={!canPost || isPosting}
                    className="btn-rounded"
                    onClick={(e) => postArticle(e)}
                >
                    {isPosting && <Spinner as="span" animation="border" variant="secondary"/>}
                    {!isPosting && 'Post'}
                </Button>
            </Modal.Body>
        </Modal>
    );
}

export default PostModal;