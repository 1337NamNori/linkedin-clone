import React, {useState} from 'react';
import {Card, Image} from "react-bootstrap";
import {useSelector} from "react-redux";
import Item from "./Item";
import Modal from "./PostModal";
import SuccessToast from "./SuccessToast";
import {selectUserAvatar} from "../../../features/auth/authSlice";
import {usePostArticle} from "../../../hooks/usePostArticle";

function PostBox(props) {
    const [showModal, setShowModal] = useState(false);
    const userAvatar = useSelector(selectUserAvatar);

    const {dispatchPostArticle, isPosting, postingDone, setPostingDone} = usePostArticle();

    const handleCloseModal = () => {
        setShowModal(false);
    }
    const handleOpenModal = () => {
        setShowModal(true);
    }

    return (
        <Card className="fw-bold fs-7 text-secondary">
            <Card.Body className="d-flex pb-1">
                <Image
                    src={userAvatar}
                    roundedCircle
                    width={48} height={48}
                />
                <div
                    className="rounded-pill border border-2 w-100 d-flex align-items-center px-3 hover-bg-gray ms-2"
                    onClick={handleOpenModal}
                >
                    Start a post
                </div>
            </Card.Body>
            <Card.Body className="d-flex flex-wrap justify-content-between align-items-center py-1">
                <Item title="Photo" image="/images/post-photo.svg" />
                <Item title="Video" image="/images/post-video.svg" />
                <Item title="Event" image="/images/post-event.svg" />
                <Item title="Write article" image="/images/post-article.svg" />
            </Card.Body>
            <Modal
                show={showModal}
                handleClose={handleCloseModal}
                isPosting={isPosting}
                postingDone={postingDone}
                setPostingDone={setPostingDone}
                dispatchPostArticle={dispatchPostArticle}
            />
            <SuccessToast
                postingDone={postingDone}
            />
        </Card>
    );
}

export default PostBox;