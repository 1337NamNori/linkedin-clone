import React, {useState} from 'react';
import {Dropdown, NavItem} from "react-bootstrap";

const sorts = ["Top", "Recent"]

function SortBar() {
    const [sort, setSort] = useState(sorts[0]);

    return (
        <Dropdown className="w-100 cursor-pointer">
            <Dropdown.Toggle as={NavItem} className="d-flex align-items-center fs-8">
                <hr className="flex-grow-1 flex-shrink-1"/>
                <span className="text-muted mx-1">Sort by:</span>
                <strong>{sort}</strong>
                <i className="fa-solid fa-caret-down ms-1"/>
            </Dropdown.Toggle>
            <Dropdown.Menu align="end">
                {sorts.map(item => (
                    <Dropdown.Item
                        key={item}
                        className={item === sort && "border-2 border-start border-success"}
                        onClick={() => setSort(item)}
                    >
                        {item}
                    </Dropdown.Item>
                ))}
            </Dropdown.Menu>
        </Dropdown>
    );
}

export default SortBar;