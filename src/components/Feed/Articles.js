import React, {useEffect, useState} from 'react';
import Article from "./Article";
import {useDispatch, useSelector} from "react-redux";
import {selectArticles, setArticles} from "../../features/articles/articleSlice";
import {Spinner} from "react-bootstrap";
import db from "../../config/firebase";

function Articles() {
    const articles = useSelector(selectArticles);
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        const getArticles = async () => {
            setIsLoading(true);
            await db.collection("articles").onSnapshot(snapshot => {
                let tempArticles = snapshot.docs.map(doc => ({id: doc.id, ...doc.data()}));
                tempArticles.sort((a, b) => b.createdAt - a.createdAt);

                dispatch(setArticles(tempArticles));
            });
            setIsLoading(false);
        }

        getArticles()
            .then(() => console.log('Success'));
    }, [dispatch])

    return (
        <div>
            {isLoading && <Spinner animation="border" variant="primary"/>}
            {!isLoading && articles.map(article => (<Article key={article.id} article={article} />))}
        </div>
    );
}

export default Articles;