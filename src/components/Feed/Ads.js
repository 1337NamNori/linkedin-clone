import React from 'react';
import {Button, Container} from "react-bootstrap";

function Ads() {
    return (
        <Container className="d-flex justify-content-center mt-5 py-3">
            <div className="d-flex flex-column flex-md-row align-items-center text-center
                            text-decoration-underline fw-bold fs-7"
            >
                <Button variant="link" className="fs-7 p-0 me-1">Hiring in a hurry?</Button>
                    Find talented pros in record time with Upwork and keep business moving.
            </div>
        </Container>
    );
}

export default Ads;