import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyD81HvmvlBum0vi-6eY_Qx8EQTYAjfpP1c",
    authDomain: "linkedin-nam.firebaseapp.com",
    projectId: "linkedin-nam",
    storageBucket: "linkedin-nam.appspot.com",
    messagingSenderId: "1075661357555",
    appId: "1:1075661357555:web:8b51f2407705f19ba45286",
    measurementId: "G-W0B2XKR5VC"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();
const storage = firebase.storage();

export { auth, provider, storage };
export default db;
