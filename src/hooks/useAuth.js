import { auth, provider } from "../config/firebase";
import { setUserLogout, setUserLogin } from "../features/auth/authSlice";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

export const useAuth = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const loginHandler = () => {
        auth.signInWithPopup(provider)
            .then(result => {
                const user = result.user;
                dispatchLogin(user);
            })
    }

    const dispatchLogin = (user) => {
        dispatch(setUserLogin({
            uid: user.uid,
            name: user.displayName,
            avatar: user.photoURL,
        }));
        history.push('/feed');
    }

    const logoutHandler = () => {
        auth.signOut().then(() => {
            dispatch(setUserLogout());
            history.push('/')
        })
    }

    return {
        dispatch,
        loginHandler,
        logoutHandler,
        dispatchLogin,
    }
}
