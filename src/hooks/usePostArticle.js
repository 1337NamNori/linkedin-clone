import db, {storage} from "../config/firebase";
import {useDispatch} from "react-redux";
import {useState} from "react";
import {randomString} from "../helpers/randomString";

export const usePostArticle = () => {
    const dispatch = useDispatch();
    const [isPosting, setIsPosting] = useState(false);
    const [postingDone, setPostingDone] = useState(false);

    const postArticleAPI = (payload) => async (dispatch) => {
        setIsPosting(true);
        if (payload.image !== '') {
            const upload = storage
                .ref(`images/${randomString(10) + payload.image.name}`)
                .put(payload.image);

            upload.on(
                'state_changed',
                () => {},
                error => {
                console.log(`Error: ${error.code}`);
            }, async () => {
                const downloadURL = await upload.snapshot.ref.getDownloadURL();
                await postArticleHandle(payload, downloadURL);
            })
        } else {
            await postArticleHandle(payload)
        }
    }


    const postArticleHandle = async (payload, downloadURL = '') => {
        await db.collection("articles").add({
            author: {
                uid: payload.userID,
                name: payload.userName,
                avatar: payload.userAvatar,
            },
            video: payload.video,
            sharedImg: downloadURL,
            comments: 0,
            description: payload.description,
            createdAt: payload.createdAt,
        });
        setIsPosting(false);
        setPostingDone(true);
    }

    const dispatchPostArticle = (payload) => {
        dispatch(postArticleAPI(payload));
    }

    return {
        dispatchPostArticle,
        isPosting,
        postingDone,
        setPostingDone,
    }
}