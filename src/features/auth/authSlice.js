import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    user: {
        uid: '',
        name: '',
        avatar: '',
    }
}

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setUserLogin: (state, action) => {
            state.user.uid = action.payload.uid;
            state.user.name = action.payload.name;
            state.user.avatar = action.payload.avatar;
        },
        setUserLogout: state => {
            state.user.uid = '';
            state.user.name = '';
            state.user.avatar = '';
        },
    }
})

export const {setUserLogin, setUserLogout} = authSlice.actions;

export const selectUserID = state => state.auth.user.uid;
export const selectUserName = state => state.auth.user.name;
export const selectUserAvatar = state => state.auth.user.avatar;

export default authSlice.reducer;
