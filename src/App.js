import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Login from "./pages/Login";
import Feed from "./pages/Feed";

function App() {
  return (
    <Router>
        <Switch>
            <Route exact path="/">
                <Login />
            </Route>
            <Route exact path="/feed">
                <Feed />
            </Route>
        </Switch>
    </Router>
  );
}

export default App;
