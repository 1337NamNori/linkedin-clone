import React, {useEffect} from 'react';
import Header from "../components/Header";
import Ads from "../components/Feed/Ads";
import {Card, Col, Container, Row} from "react-bootstrap";
import Sidebar from "../components/Sidebar";
import PostBox from "../components/Feed/PostBox";
import Aside from "../components/Aside";
import Footer from "../components/Footer";
import SortBar from "../components/Feed/SortBar";
import {auth} from "../config/firebase";
import {useAuth} from "../hooks/useAuth";
import {useHistory} from "react-router-dom";
import Articles from "../components/Feed/Articles";

function Feed() {
    const {dispatchLogin} = useAuth();
    const history = useHistory();

    useEffect(() => {
        auth.onAuthStateChanged(user => {
            if (user) {
                dispatchLogin(user);
            } else {
                history.push('/');
            }
        })
    }, [dispatchLogin, history]);

    return (
        <div className="bg-gray">
            <Header />
            <Ads/>
            <Container>
                <Row>
                    <Col sm={12} md={4} lg={3}>
                        <Sidebar />
                    </Col>
                    <Col sm={12} md={8} lg={9}>
                        <Row>
                            <Col md={12} lg={8}>
                                <PostBox />
                                <SortBar />
                                <Articles />
                            </Col>
                            <Col md={12} lg={4}>
                                <Aside />
                                <Card className="mt-2 p-1"/>
                                <Footer />
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default Feed;