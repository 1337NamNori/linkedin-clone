import React, {useEffect} from 'react';
import {Button, Col, Container, Image, Nav, Navbar, Row} from "react-bootstrap";
import {useAuth} from "../hooks/useAuth";
import {auth} from "../config/firebase";

function Login() {
    const {loginHandler, dispatchLogin} = useAuth();

    useEffect(() => {
        auth.onAuthStateChanged(user => {
            if (user) dispatchLogin(user);
        })
    }, [dispatchLogin]);

    return (
        <div>
            <Navbar bg="white" expand="sm">
                <Container>
                    <Navbar.Brand>
                        <Image src="/images/login-logo.svg" alt="" style={{height: "30px"}}/>
                    </Navbar.Brand>
                    <Navbar.Toggle/>
                    <Navbar.Collapse className="justify-content-end">
                        <Nav>
                            <Button variant="secondary" className="btn-text me-sm-3">Join now</Button>
                            <Button variant="primary" className="btn-rounded">Sign in</Button>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <Container className="mt-5">
                <Row>
                    <Col xs={12} md={6} className="d-flex flex-column align-items-center d-md-block">
                        <h1 className="display-4 fw-light text-md-start text-center" style={{color: "#8f5849"}}>
                            Welcome to your professional community
                        </h1>
                        <Button
                            variant="secondary"
                            className="btn-rounded btn-lg fw-normal mt-4 mw-100"
                            style={{width: "350px"}}
                            onClick={loginHandler}
                        >
                            <Image src="/images/google.svg" alt="" className="me-3"/>
                            Sign in with Google
                        </Button>
                    </Col>
                    <Col xs={12} md={6} className="mt-5 mt-md-0">
                        <Image src="/images/login-hero.svg" alt="" fluid/>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default Login;